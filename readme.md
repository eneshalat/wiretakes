    fn main(){ 
	    println!("Welcome! Glad to have you here");
    }

### wiretakes
Currently high school graduate, interested in programming and open source, tinkering with Linux around 5 years, ∞ code love!
## Welcome aboard!
Hi! I am wiretakes and I'm here to learn coding better and also share my software ideas. I've been into programming for 7 years, and I love tinkering around with them since I was born. :D

### What will you find here?
> You will find my latest repositories and ideas. They will be shared here as soon as I start to create them.
> You will find many mistakes and many attempts to create a working program, most developers don't like to share them, but I do! That's the 'open source' spirit! :) I will share them here because they will be a great memory when I want to go on some nostalgia tour, and also I will be learning to use Git better in the process.  

### Which programming languages are in your radar right now?
I am currently tinkering with:

 - Rust
 - C++
 - Python
 - Javascript

My favorite full-stack is MERN btw! :D

### How can I contact you? 
You can contact me on matrix.org! I am pretty active in there because that's my main programming and open source base. And also when I get into college, you'll be able to contact me on LinkedIn for business purposes. 


### That's the end!
Thank you for looking to my profile page! If any of my software ideas inspire you or you think you can make that idea stronger, always feel free to contribute! 

*2021 - wiretakes*
